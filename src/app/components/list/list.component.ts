import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Todo } from './../../models/todo.model';
import { AppState } from './../../app.state';
import * as TodoActions from './../../actions/todo.actions';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  public todoList: Observable<Todo[]>;
  private todoListSubscription: Subscription;

  public todos: FormArray;
  public todoForm: FormGroup;

  public itemsLeft: string;

  get todoControls() {
    return this.todoForm.get('todos')['controls'];
  }

  constructor(private store: Store<AppState>, private fb: FormBuilder) {
    this.todoList = store.select('todo');
    this.todoForm = this.fb.group({
      todos: this.fb.array([])
    });
  }

  private createTodoField(data: Todo): FormGroup {
    return this.fb.group({
      check: [data.check],
      description: [data.description]
    });
  }

  public ngOnInit(): void {
    this.todos = this.todoForm.get('todos') as FormArray;

    this.todoListSubscription = this.todoList?.subscribe((todoItemList: Todo[]) => {
      this.clearFormArray();

      todoItemList?.forEach((todoItem: Todo) => {
        this.todos.push(this.createTodoField(todoItem));
      });

      this.countItems();
    })
  }

  public clearFormArray(): void {
    while (this.todos.length > 0) {
      this.todos.removeAt(0);
    }
  }

  public countItems(): void {
    const OUTSTANDING_LIST = this.todoForm.value.todos.filter((todoItem: Todo) => todoItem.check === false);
    const ITEM_TEXT = OUTSTANDING_LIST?.length > 1 || OUTSTANDING_LIST?.length === 0 ? 'items' : 'item';
    this.itemsLeft = `${OUTSTANDING_LIST?.length} ${ITEM_TEXT} left`;
  }

  public updateCheck(index: number): void {
    const FORM_ARRAY = this.todoForm.controls.todos as FormArray;
    const FORM_ARRAY_ITEM_CHECK = (FORM_ARRAY).at(index).get('check');
    const FORM_ARRAY_ITEM_DESCRIPTION = (FORM_ARRAY).at(index).get('description');
    const CURRENT_VALUE = (FORM_ARRAY_ITEM_CHECK.value) as boolean;
    const UPDATED_ITEM = {
      check: !CURRENT_VALUE,
      description: FORM_ARRAY_ITEM_DESCRIPTION.value
    };

    this.store.dispatch(new TodoActions.UpdateTodo(UPDATED_ITEM, index));
  }

  public deleteTodo(index: number): void {
    this.store.dispatch(new TodoActions.RemoveTodo(index));
  }

  public ngOnDestroy(): void {
    this.todoListSubscription.unsubscribe();
  }
}
