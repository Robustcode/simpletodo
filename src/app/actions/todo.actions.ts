import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Todo } from './../models/todo.model';

export const ADD_TODO      = '[TODO] Add';
export const UPDATE_TODO    = '[TODO] Update';
export const REMOVE_TODO    = '[TODO] Remove';

export class AddTodo implements Action {
    readonly type = ADD_TODO;

    constructor(public payload: Todo) {};
}

export class UpdateTodo implements Action {
    readonly type = UPDATE_TODO;

    constructor(public payload: Todo, public index: number) {};
}

export class RemoveTodo implements Action {
    readonly type = REMOVE_TODO;

    constructor(public payload: number) {};
}

export type Actions = AddTodo | UpdateTodo | RemoveTodo;