import { Action } from '@ngrx/store';
import { Todo } from './../models/todo.model';
import * as TodoActions from './../actions/todo.actions';

const INITIAL_STATE: Todo = {
    check: false,
    description: 'Do some work'
};

export function reducer(state: Todo[] = [INITIAL_STATE], action: TodoActions.Actions) {

    switch(action.type) {
        case TodoActions.ADD_TODO:
            return [...state, action.payload];
        case TodoActions.UPDATE_TODO:
            state = [...state];
            state.splice(action.index, 1, action.payload);
            return state;
        case TodoActions.REMOVE_TODO:
            state = [...state];
            state.splice(action.payload, 1);
            return state;
        default:
            return state;
    }
}