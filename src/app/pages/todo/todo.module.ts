import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoComponent } from './todo.component';
import { InputModule } from 'src/app/components/input/input.module';
import { ListModule } from 'src/app/components/list/list.module';

@NgModule({
  declarations: [TodoComponent],
  imports: [
    CommonModule,
    InputModule,
    ListModule
  ]
})
export class TodoModule { }
