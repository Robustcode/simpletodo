import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.state';
import * as TodoActions from './../../actions/todo.actions';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, AfterViewInit {
  @ViewChild('field') public field: ElementRef<HTMLInputElement>;
  public valid: boolean = true;

  constructor(private store: Store<AppState>) { }

  public ngOnInit(): void {
  }

  public ngAfterViewInit(): void {
    this.field.nativeElement.focus();
  }

  public addTodo(check: boolean, description: string): void {
    if (!!description?.trim()) {
      this.valid = true;
      this.store.dispatch(new TodoActions.AddTodo({check: check, description: description}));
      this.clearField();
    } else {
      this.valid = false;
    }
  }

  public clearField(): void {
    if (!!this.field) {
      this.field.nativeElement.value = '';
      this.valid = true;
    }
  }

}
