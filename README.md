## SimpleTodo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Installing dependencies

Make sure you have angular-cli `npm install -g @angular/cli` and nodejs `nodejs.org` installed

Clone and open the project with your preferred IDE like vs-code then run `npm install` to install all the app dependencies...
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## About the App

* This application is a simple todo list generator. It's aim is to keep track of your tasks as you complete them and at the same time be able to make and manage a todo list with the use of your keyboard and or mouse. It contains an items left section which tracks unchecked items as outstanding tasks. The list is not saved so when the app gets refreshed it clears all items except the initially added one. A quick view of the app can be found on the following link `https://simpletodolist.vercel.app/`.

(One can select items on the app using `tab` and `enter` key to proceed with the instruction, to reach the crosses for clearing or deleting items it takes a couple of tab attempts but it works)

* Only unchecked items can be deleted from the list. Once an item is checked, the delete button corresponding to it gets hidden.

* I have added some validation to make sure added items have atleast one character, if the input is empty or contains only spaces, an error message will display on submission.

* I also added a clear button on the input which shows up when anything is typed in the input.

* Since the list has checkboxes and it's dynamic i couldn't use ngModule, i had to use reactive forms as they are easy to handle when dealing with dynamic forms.

* I also used Ngrx store to manage the data, this helps a lot when dealing with a lot of data across multiple components.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
